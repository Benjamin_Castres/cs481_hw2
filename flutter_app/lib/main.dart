import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fisrt Assignment',
      theme: ThemeData(
        primarySwatch: Colors.red
      ),
      home: MyHomePage(
        title: 'HomeWork 2',
      )
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {

    Widget titleWidget = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.all(5),
            child: Text("Champions League - Paris Saint Germain",
              style: TextStyle(
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(left: 5),
            child: Text(
              'Neymar left in tears after PSG suffer Champions League\nfinal heartbreak (08/25/2020)',
              style: TextStyle(
                color: Colors.grey[500],
              ),
            ),
          ),
        ],
      ),
    );

    Widget textOverviewWidget = Container(
      padding: EdgeInsets.all(10),
      child: Text(
        'Estádio do Sport Lisboa e Benfica in Lisbon (Portugal) hosted the 2019/20 UEFA Champions League final on Sunday 23 August at 9 p.m. '
            'Also named Estádio da Luz, the lair of 37-time Portuguese champion Benfica was rebuilt for UEFA EURO 2004 and was home to the 2014'
            ' UEFA Champions League final, Real Madrid\'s 4-1 victory after extra time against neighbors Atlético for La Décima des Merengues. '
            'Paris landed the first ticket to the final (the first final in the club\'s history) by winning against Leipzig in the semi-final (3-0). '
            'He faced Bayern, who eliminated Lyon in the second semi-final.'
      ),
    );

    Widget textStatWidget = Container(
      padding: EdgeInsets.all(10),
      child: Text(
          'Paris Saint-Germain star Neymar was left in tears after his side suffered a 1-0 Champions League final loss to Bayern Munich in Lisbon.'
              'The Ligue 1 champions succumbed to their German opponents due to a Kingsley Coman header shortly before the hour mark, and the 28-year-old'
              'was unable to inspire a comeback as Bayern kept the French outfit at arm’s length. eymar, who was consoled in the dugout by sporting '
              'director Leonardo at full-time, had one of the outstanding opportunities in the opening period of the game but was denied by an inspired '
              'Manuel Neuer, who also stopped a tame effort from Kylian Mbappe before the break.'
      ),
    );

    Column _buildButtonWidget(Color color, IconData icon, String label) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Icon(icon, color: color),
          Container(
            margin: const EdgeInsets.only(top: 3),
            child: Text(label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
            ),
          ),
        ],
      );
    }

    Widget buttonOverviewWidget = Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: <Widget>[
          Container(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _buildButtonWidget(Colors.red, Icons.not_listed_location, 'Location: Lisbonne'),
              _buildButtonWidget(Colors.red, Icons.looks_one, 'Background: 1st Final'),
              _buildButtonWidget(Colors.red, Icons.euro_symbol, 'Reward: 134 M€'),
            ],
          ),
        ],
      ),
    );

    Widget buttonStatWidget = Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: <Widget>[
          Container(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _buildButtonWidget(Colors.red, Icons.person_add, 'Top player: NeymarJR'),
              _buildButtonWidget(Colors.red, Icons.person_outline, 'Flop player: Di Maria'),
              _buildButtonWidget(Colors.red, Icons.star, 'Huge opportunity: 4'),
            ],
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: <Widget>[
          Image.asset('image/psg.jpg',
            width: 600,
            height: 240,
            fit: BoxFit.cover,
          ),
          titleWidget,
          buttonOverviewWidget,
          textOverviewWidget,
          buttonStatWidget,
          textStatWidget,
        ],
      ),
    );
  }
}
